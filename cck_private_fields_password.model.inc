<?php 
/**
 * @file
 * Database Handlers
 */

/**
 * Loads a cck access type setting for a node.
 *
 */
function cck_private_fields_password_load_cck_access_type($nid){
	return db_fetch_object(db_query("SELECT access_type, password FROM {node_cck_access} WHERE nid=%d LIMIT 1", $nid));
}

/**
 * Saves or updates the cck access type settings for a node.
 *
 * @param object $data
 *   An object that maps over {node_cck_access} table
 */
function cck_private_fields_password_save_cck_access_type($data){
	//First, delete the old entry if it exists.
	db_query("DELETE FROM {node_cck_access} WHERE nid=%d LIMIT 1", $data->nid);
	db_query("INSERT INTO {node_cck_access} (nid, access_type, password) VALUES(%d, %d, '%s')", $data->nid, $data->access_type, $data->password);
}

/**
 * Checks if there is another node with the same password.
 *
 */
function cck_private_fields_password_check_if_password_exists($nid, $password){
	if (is_object($nid)){
		$nid = $nid->nid;
	}
	if (empty($password)){
		return FALSE;
	}
	//This will work only if content_field_cck_private_field_password is a table.
	return db_result(db_query("SELECT nid FROM {node_cck_access} WHERE nid <> %d AND password='%s'", $nid, $password));
}

/**
 * Loads the nid of a node by a password.
 *
 */
function cck_private_fields_password_load_by_password($password){
	return db_result(db_query("SELECT nid FROM {node_cck_access} WHERE password='%s' LIMIT 1", $password));
}