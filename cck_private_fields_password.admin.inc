<?php 
/**
 * @file
 * Implementation of Admin Settings
 */
 
function cck_private_fields_password_admin() {
  $form['cck_private_fields_password_distinct'] = array(
    '#type' => 'checkbox',
    '#title' => t('Distinct password on every node'),
    '#default_value' => variable_get('cck_private_fields_password_distinct', NULL),
  );
  return system_settings_form($form);
}