<?php 
/**
 * @file
 * External helpers for other modules
 */
 
/**
 * Checks if the node sent as parameter has private data inserted in his cck fields.
 *
 */
function cck_private_fields_password_node_has_private_data($node, $reset = TRUE) {
  static $nodes;
  //Make sure the $node is an object.
  if (!is_object($node)) {
    $node = node_load($node);
  }
  if (!isset($nodes[$node->nid]) || $reset) {
    $fields = content_fields();
    foreach ($fields as $key => $value) {
      $function = $value['module'] . '_content_is_empty';
      if (count($node->{$key})) {
        foreach ($node->{$key} as $_key => $_value) {
          $empty_field = module_invoke($value['module'], 'content_is_empty', $_value, $value);
          //If the field is not empty and it it private, then return 1.
          if (!$empty_field && cck_private_fields_get_field_privacy_status($node->vid, $key) == CCK_FIELDS_PRIVACY_STATUS_PRIVATE) {
            $nodes[$node->nid] = 1;
            return $nodes[$node->nid];
          }
        }
      }
    }
    $nodes[$node->nid] = 0;
    return $nodes[$node->nid];
  }
  return $nodes[$node->nid];
}