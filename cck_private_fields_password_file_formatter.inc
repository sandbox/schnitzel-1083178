<?php

/**
 * @file
 * Custom theme functions for showing the link to the private files.
 *
 */

function theme_cck_private_fields_password_formatter_default($element) {
  $file = $element['#item'];
  $field = content_fields($element['#field_name']);
  $output = theme('cck_private_fields_password_filefield_item', $file, $field);
  return $output;
}

function theme_cck_private_fields_password_filefield_item($file, $field) {
  if (filefield_view_access($field['field_name']) && cck_private_fields_password_file_listed($file, $field)) {
    return theme('cck_private_fields_password_file', $file);
  }
  return '';
}

function theme_cck_private_fields_password_file($file) {
  // Views may call this function with a NULL value, return an empty string.
  if (empty($file['fid'])) {
    return '';
  }

  $path = $file['filepath'];
  $url = url($path, array('absolute' => TRUE));
  $icon = theme('filefield_icon', $file);

  // Use the description as the link text if available.
  if (empty($file['data']['description'])) {
    $link_text = $file['filename'];
  }
  else {
    $link_text = $file['data']['description'];
    $options['attributes']['title'] = $file['filename'];
  }

  return '<div class="filefield-file">'. $icon . l($link_text, $url) .'</div>';
}

function cck_private_fields_password_file_listed($file, $field) {
  if (!empty($field['list_field'])) {
    return !empty($file['list']);
  }
  return TRUE;
}